package mn.collatz.neo4j

import grails.gorm.annotation.Entity
import grails.neo4j.Node

@Entity
class Collatz implements Node<Collatz> {

    long value

    int deep

    Collatz parent

    int odd
    int even

    static constraints = {
        parent nullable:true
    }

    static mapping = {
        id generator:'assigned', name:'value'
        parent index:true
    }

}
