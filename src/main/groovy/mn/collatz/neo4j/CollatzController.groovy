package mn.collatz.neo4j

import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.reactivex.Single

import javax.inject.Inject

@Controller('/')
class CollatzController {

    CollatzService collatzService

    @Inject
    CollatzController(CollatzService collatzService){
        this.collatzService = collatzService
    }


    //tag::add[]
    @Get('/generate/{max}')
    Single<Integer>generate( long max){
        Single.create{ emitter ->
            emitter.onSuccess(collatzService.add(max))
        }
    }
    //end::add[]
}
