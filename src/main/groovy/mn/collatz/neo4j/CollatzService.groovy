package mn.collatz.neo4j

import groovy.transform.Memoized

import javax.inject.Singleton

@Singleton
class CollatzService {

    //tag::add[]
    int add(long toGenerate){
        List last = Collatz.listOrderByValue(max:1, order: "desc")

        long next = last ? last.first().value : 1

        (0..toGenerate-1).each {
            Collatz.withTransaction {
                Collatz node = buildCollatz(++next)
                node.save()
            }
        }

        toGenerate
    }
    //end::add[]

    //tag::buildCollatz[]
    Collatz buildCollatz(long seed, int deep=3000){

        Collatz node
        if( (node = Collatz.findByValue(seed)) )
            return node

        node = new Collatz(value: seed)

        long n = seed

        int i=0;

        while( i++ < deep ){
            if( n % 2 == 0){
                n = n / 2
                node.odd++
            }else{
                n = (n*3)+1
                node.even++
            }
            if(n == 1)
                break
            if( !node.parent ){
                node.parent = buildCollatz(n,deep)
            }
        }
        node.deep = i
        node
    }
    //end::buildCollatz[]
}
